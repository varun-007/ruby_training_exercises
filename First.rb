# Count the occurrences of various alphabets in an input string and store it in hash.


def count_letters(s)
    Hash[s.delete(' ').split('').group_by{ |c| c }.map{ |k, v| [k, v.size] }]
end
puts("ENTER THE STRING")
name = gets
print count_letters(name)