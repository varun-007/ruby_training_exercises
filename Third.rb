# Print fibonacci series upto 1000 using 'yield'.

def fibonacci(limit = nil)
  seed1 = 0
  seed2 = 1
  while not limit or seed2 <= limit
    yield seed2
    seed1, seed2 = seed2, seed1 + seed2
 end
end

fibonacci(1000) { |x| puts x }