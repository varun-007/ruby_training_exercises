# Ask user to enter text. Replace each vowel in the text with a '*' using regular expression

def vowel_censor(string)
  string.tr("aeiou", "*") # lowercase only; use "aeiouAEIOU" to handle both upper- and lowercase
end

puts("enter the word")
name=gets
print vowel_censor(name)